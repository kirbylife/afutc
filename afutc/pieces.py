import numpy as np

PIECES = [[[0, 1], [0, 1], [1, 1]], [[0, 1, 0], [1, 1, 1]], [[1, 1], [1, 1]],
          [[1, 1, 0], [0, 1, 1]], [[0, 1, 1], [1, 1, 0]], [[1], [1], [1], [1]],
          [[0, 1, 0], [0, 1, 0], [1, 1, 1]]]

# To see the color number check this link:
# https://i.stack.imgur.com/7AtMc.png
__colors = [5, 7, 9, 11, 13, 15, 35, 41, 123, 205, 219]

for n, piece in enumerate(PIECES):
    piece = np.array(piece)
    piece *= __colors[n]
    PIECES[n] = piece.copy()
